# резюме


 * https://www.linkedin.com/advice/0/how-does-ats-score-your-resume-skills-resume-writing
 * https://www.open-resume.com/resume-parser

## разбор видео

 * [как выделить резюме для бигтеха 2023](https://www.youtube.com/watch?v=i-6l4cI7poY&t=20188s)
 * рекрутеры просматривают 100-200 резюме в день
	* допустим, 4 часа на встречи, 4 часа на просмотр и фильтрацию резюме
	* итого резюме должно заинтересовать после 1-3 минуты чтения
 * В CV должна быть указана короткая крутая история успеха
	* в резюме должны быть видны попадания в ключевые параметры вакансии
	* ключевой момент - принятие решения на предварительный собес или продвижение дальше по воронке скрининга
	* полезно сформулировать CV или follow-up как обоснование почему я подхожу на конкретную вакансию в конкретную компанию. Тогда этот текст рекрутер сможет переслать дальше, не тратя время на обдумывание.
