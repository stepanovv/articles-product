# проектное интервью Продакт

## ЧАВО

 * что это
	* анкета проектного интервью
    * данные для ранжирования качества вакансии/проекта
 * для кого это
	* заполняется кандидатом по результатам интервью с командой проекта или руководителем проекта
 * для чего это
    * для проведения интервью с менторами-фрилансерами и с командой проекта
    * на входе нужен чёткий список личных предпочтений на каждый вопрос
    * на выходе карта минных полей на испытательном сроке
 * что не вошло в вопросы и почему
    * индексация зп. Если она есть, про неё обязательно скажут
    * что происходит, если сроки превышены? Болевая точка.
 * ограничения
	* ответы на вопросы необходимо сильно интерпретировать, т.к. вам не скажут всей правды, и смягчат формулировки.
	* ответы однозначно проверяются/валидируются только после погружения в проект. Такова жизнь.
 * как считать баллы, легенда
	* баллы от 0 до 6
	* балл 0 - отсутствие информации
	* чем больше балл, тем больше вероятность успешно пройти испытательный срок

## обучение

* Кто и как помогает пройти испытательный срок?
    1. сам разберёшься
    2. вот инструкция, потом сходишь на 1-1
    3. вот тебе наставник, вот план вхождения в должность
* Приведите пример типичных ошибок на испытательном сроке, сколько их допускают, как исправляют?
    1. никто не поможет
    2. помогут обратить внимание на ошибку
    3. после каждой ошибки составляем план исправления ситуации, помогаем исправить
* Приведите примеры увольнения с испытательного срока, как это происходило, какие выводы сделали?
    1 плохо работали, наняли других
    3 поменяли процессы найма и адаптации
    6 не увольняли, помогли справиться с проблемами

## финансы

 * Приведите примеры закупки инструментов и услуг для исследования рынка, что закупалось, кто и как выделял бюджет?
    1. используйте бесплатные данные, смотрите в логи
    2. меньше 1 оклада в год или уже всё куплено
    3. больше 1 оклада в год или согласуется под гипотезу
 * Приведите пример карьерного проста продакта, как росли обязанности и возможности, как согласовывали компенсацию?
    1. яхз
    2. есть грейды
        * навыки аналитики
        * знания теории
        * навыки интервью
        * объём и сроки выполнения работы
    3. есть грейды и планы развития

## процессы

* Какие зоны ответственности у продакта, возможности и ограничения? Команда, бюджеты, приоритеты.
    1 отвечает за то, чем не управляет
    3 отвечает за продукт частично, полномочий достаточно
    6 отвечает полностью за продукт, mini-CEO
* Есть ли сложные люди или процессы внутри или снаружи команды, как с ними работать?
    1. конфликты за сроки, межличностные все против всех
    2. конфликты за бюджеты, смешанные эмоциональные
    3. конфликты за философию, конструктивные
* Приведите пример как работает петля обратной связи от менеджмента, команды, пользователей? Периодичность, объём, формат?
    1. не работает, формализм, ни на что не влияет
    2. работает с трудом
    3. примеры с методикой и автоматизацией
* Какая типичная скорость проверки гипотез на 1 аналитика/продакта
	1. 1 гипотеза за 1- спринт
	2. 1 гипотеза за 2+ спринта
	3. разные гипотезы по-разному, но не более 1 гипотезы за 2+ спринта
* Из чего состоит проверка гипотез? Этапы, артефакты, команды?
	1. сам сусам, без бюджета и команды
	2. самостоятельная аналитика и прототипы в команде
	3. руководство командами аналитики, разработки

## сроки, планирование

* Бывало ли так, что задачи прилетали внезапно и с горящими сроками? Как часто, какие?
    1 постоянно. любые. мы работаем 24/7
    3 регулярно. по определённой тяжёлой теме/области
    6 редко. мы с ними успешно боремся
* Как и с кем вы согласуете сроки выполнения задач?
    1. назначает заказчик
    2. совместно с командой/заказчиком
    3. отталкиваемся от возможностей команды(kanban)
* Приведите пример, когда не успели в срок, как часто это происходит, какие варианты действий?
    1. постоянно ставят невыполнимые задачи, гореть в аврале
    2. по определённой тяжёлой теме/области, менять приоритеты
    3. редко, сроки можно двигать
* Опишите ваш типичный рабочий день, типичные регулярные задачи
    1 9+ часов, встречи более 50% времени
    3 9- часов, встречи менее 50% времени
    6 8- часов
        * 0.5 - Планирование рабочего дня
        * 1.5 - Чтение и ответы в почту/чат
        * 2.0 - встречи: статус, планирование, согласование ТЗ
        * 2.0 - Анализ метрик/логов/отзывов по продукту
        * 2.0 - Отчёты/презентации
* Какой горизонт планирования, какие нужны исходные данные?
    1. 1 спринт или меньше, чуйка
    2. месяц, жира
    3. квартал/год, жира+данные по рынкам и соседним отделам
* Были ли случаи, когда не соглашались с заказчиком, что потом произошло? Какое отношение к конструктивной критике?
    1 увольняется
    3 обсуждается
    6 поощряется
* Какие решения или проблемы откладывают на потом, почему?
    1. по многим направлениям работы, некогда
    2. по 1 направлению, временные ограничения
    3. все проблемы решаем своевременно

## рабочая среда

 * Есть ли сложные/уникальные инструменты/методики внутри/снаружи команды, как с ними работать?
	1. нет
	2. есть документация и эксперты
	3. проводится обучение


