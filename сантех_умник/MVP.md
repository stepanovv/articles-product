# MVP, Жизненный цикл продукта

Здесь рассмотрены разные аспекты создания MVP: продуктовые, маркетинговые, бизнес-финансовые, технические, организационные, юридические.

Затронуты более поздние этапы жизненного цикла продукта, включая пилотную эксплуатацию, внедрение и масштабирование. На каждом из этих этапов будут протестированы различные гипотезы, будут созданы свои MVP частей или свойств продукта.

## путь к вершине восторга и успеха

 * создать сообщество сантех самоучек
 * минимум программирования
 * найти lowcode/nocode платформу для web+3d
 * найти партнёров для тестирования продукта онлайн
 * найти минимальный объём функционала по статистике продаж комплектующих в конкретном магазине-партнёре

## план-график

### Предварительные исследования

 * Они включают исследования рынка сбыта, опрос потенциальных пользователей, подготовку аналитических документов.
 * Исследования выполнены, потенциал в развитии продукта на текущем рынке присутствует.
 * Артефакты:
	* Юнит-экономика, PnL
	* материалы Custdev
	* дизайн и идея продукта
	* посадочные страницы для custdev, MVP

### 1. Разработка онлайн-сервиса

 * Сроки: 3-6 месяцев при частичной занятости
 * Условие начала этапа: собраны минимальные доказательства жизнеспособности финансовой модели на текущем рынке
 * Без инвестиций, в свободное от основной работы время, под лицензией open source
 * Рассчёты юнит-экономики не нужны, т.к. нет привлечения пользователей и взаимодействия с партнёрами и заказчиками
 * В первую очередь стоит позаботиться об удобстве и лёгкости обучения пользователей работе с конфигуратором, сконцентрировать усилия на одном самом ходовом типе систем водоснабжения
 * Главна побочная задача: ограничение функционала для уменьшения проблем перехода от бесплатной к платной модели
 * Признаки завершения этапа
	* Создан онлайн сервис с возможностью масштабирования до 1 тыс человек онлайн
	* Созданы инструменты привлечения и удержания: удобный интерфейс, учебные материалы, обратная связь от сообщества пользователей
	* С помощью сервиса спроектированы и реализованы от 10 до 100 проектов инженерной сантехники
	* Юридическая защита от патентных троллей: юрлицо, торговые знаки и особенности технологии продукта
	* Сделаны расчёты предельных нагрузок на инфраструктуру при масштабировании до 10 тыс человек онлайн

### 2. Разработка встраиваемого решения

 * сроки 6-12 месяцев при полной занятости
 * Условие начала этапа: привлечение инвестиций 0,5-1 млн руб
 * Необходимо соблюдать контрактные обязательства по разработке интеграции с крупными торговыми площадками, решать технические, финансовые и организационные проблемы найма линейного персонала
 * Здесь пригодятся расчёты юнит-экономики, будут вкладываться деньги в привлечение пользователей и найм
 * В первую очередь стоит сосредоточиться на гибкости органиации обмена данными про запчасти с внутренними системами магазина. Здесь появляется функционал загрузки данных о запчастях из магазинов и отправка данных о выбранных запчастях в корзину магазина
 * Главная побочная задача: защита возможности масштабирования на несколько магазинов и собственный онлайн сервис
 * Признаки завершения этапа
	* Создан онлайн сервис и встраиваемый виджет с возможностью масштабирования до 10 тыс человек онлайн
	* Созданы инструменты привлечения и удержания: регулярные маркетинговые исследования, регулярное обновление бизнес-логики взаимодействия запчастей, регулярная верификация бизнес-логики и учебных материалов экспертами сантехниками
	* С помощью сервиса спроектированы и реализованы от 1000 до 10000 проектов инженерной сантехники
	* Юридическая защита от партнёров и заказчиков: ООО, масштабирование на несколько магазинов и собственный онлайн сервис
	* Создана техническая защита: мониторинг, ИБ, резервирование
	* Сделаны расчёты юнит-экономики и анализ рисков масштабирования продукта/команды/финансов
	* Сделаны расчёты предельных нагрузок на инфраструктуру при масштабировании до 100 тыс человек онлайн

### 3. Масштабирование

 * Сроки: 12-24 месяца при полной занятости и делегировании части задач
 * Условие начала этапа: привлечение инвестиций 10-100 млн руб, регистрация 
 * Необходимо соблюдать контрактные обязательства по разработке интеграции с несколькими крупными торговыми площадками, решать технические, финансовые и организационные проблемы найма руководителей и линейного персонала
 * Необходимо учитывать риски прекращения финансирования и внезапного разрыва контрактов на интеграцию решений
 * В первую очередь стоит сосредоточиться на расширении функционала в другие инженерные системы, и росте числа подключённых магазинов
 * Главная побочная задача: сохранение качества продукта на приемлемом уровне в условиях масштабирования ассортимента запчастей, аудитории и источников данных, поддержание гибкости кода и структур данных в быстрорастущей кодовой базе продукта
 * Признаки завершения этапа
	* Онлайн сервис и встраиваемый виджет обеспечивают работу с несколькими магазинами и до 100 тыс человек онлайн
	* Созданы инструменты привлечения и удержания: разные варианты повышения среднего чека и общей прибыли, системы лояльности в партнёрстве с магазинами
	* Юридическая защита от недружественных действий партнёров и конкурентов: шантаж, демпинг, подкуп
	* Создана техническая защита: собственная железная инфраструктура в ЦОД, географически разнесённое резервирование

## Гипотезы

 * Критерии проверки MVP когда MVP успешен, а когда нет;
	* [Пример того, как описывать MVP](https://docs.google.com/document/d/1ZU6znxhJ-3R5XpguT2zjBv4gdxdfV5FYFsS0ODqkfZs/edit)
 * TODO 3 гипотезы, расписанные по методу HARDI. 
 	* Обязательно отметьте в цикле условие, при котором гипотеза подтверждена, и условие, при котором гипотеза проверена.
	* Помните, что гипотезы по HARDI — это влияние на какую-то метрику, а не продуктовые гипотезы. 
	* Т. е. вначале решите, на какие метрики надо повлиять, потом выдвигайте гипотезы по улучшению этих метрик;

### этап 1

 * Онлайн сервис может выдержать нагрузку 1 тыс человек онлайн
 * Пользователи считают интерфейс удобным
 * Пользователи считают учебные материалы достаточными и лёгкими в освоении
 * Сообщество пользователей охотно даёт обратную связь и участвует в развитии продукта
 * Удалось привлечь 100 бесплатных пользователей
 * Сервис решает проблемы пользователей, спроектированы и реализованы от 10 до 100 проектов инженерной сантехники
 * Оказалось возможным без существенного бюджета юридически защитить торговые знаки и особенности технологии продукта от патентных троллей
 * Исходя из собранной статистики мониторинга понятна предельная нагрузка на инфраструктуру при масштабировании до 10 тыс человек онлайн

### этап 2

 * Онлайн сервис и встраиваемый виджет могут выдержать нагрузку до 10 тыс человек онлайн
 * Регулярные маркетинговые исследования поставляют верифицируемые данные 
 * Регулярное обновление бизнес-логики взаимодействия запчастей не ломает функционал, рабочие процессы и производительность решения
 * Эксперты сантехники могут помогать верифицировать бизнес-логику и учебные материалы, предотвращать ошибки проектирования у пользователей
 * Удалось привлечь более 25 тыс платных пользователей
 * Сервис решает проблемы пользователей, спроектированы и реализованы от 1000 до 25000 проектов инженерной сантехники
 * Оказалось возможным с ограниченным бюджетом юридически защитить процесс масштабирования от недружественных действий партнёров и заказчиков
 * Оказалось возможным с ограниченным бюджетом технически защитить инфраструктуру и данные от сбоев и внешних атак
 * Оказалось возможным с ограниченным бюджетом рассчитать юнит-экономику и подготовить анализ рисков масштабирования продукта/команды/финансов
 * Исходя из собранной статистики мониторинга понятна предельная нагрузка на инфраструктуру при масштабировании до 100 тыс человек онлайн

### этап 3

 * Онлайн сервис и встраиваемый виджет могут выдержать нагрузку с несколькими магазинами и до 100 тыс человек онлайн
 * Созданы инструменты привлечения и удержания: разные варианты повышения среднего чека и общей прибыли, системы лояльности в партнёрстве с магазинами
 * Юридическая защита от недружественных действий партнёров и конкурентов: шантаж, демпинг, подкуп
 * Создана техническая защита: собственная железная инфраструктура в ЦОД, географически разнесённое резервирование


## Описание прототипа

 * Что собой представляет MVP (обоснуйте, почему именно так);
 TODO закончить LEAN, много информации дублируется
 * Бизнес-модель Lean Canvas (не модель Остервальдера).
	* [Полезная статья про нерыночное конкурентное преимущество](http://ask.leanstack.com/en/articles/904720-what-is-an-unfair-advantage)
 * [](./CANVAS.md) 
 * [дизайн](./дизайн.md)


