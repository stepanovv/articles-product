# аналитика и продуктовые метрики

## ссылки

 * Retention
    * [Using cohort analysis to improve retention](https://www.intercom.com/blog/retention-cohorts-and-visualisations/) — как визуализировать данные
    * Сравнить свой Retention c [отчётом Mixpanel](https://discover.mixpanel.com/rs/461-OYV-624/images/2017-Mixpanel-Product-Benchmarks-Report.pdf) по отраслям
    * [Retentioneering](https://www.youtube.com/watch?v=C6dHFBX5Ig8&t=2255s) — фреймворк для поиска инсайтов
 * LTV
    * [блог](https://ecommerce-in-ukraine.blogspot.com/search/label/LTV) на русском языке с разными подходами к прогнозированию LTV
    * [Lifetime Value: главная метрика проекта](https://www.devtodev.com/education/ebooks/ru/114/lifetime-value-glavnaya-metrika-proekta) — книга про LTV от devtodev (требуется регистрация)
    * [Прогнозные модели в Excel](https://vc.ru/16024-ltv-excel) от Алексея Куличевского
 * NPS
    * видео [NPS Mail.ru](https://www.youtube.com/watch?v=JvSLoh62ZZ8&list=LL&index=16&t=1s) — Анна Ошарова о развитии сбора информации вместе с NPS
 * иерархия
    * https://productstar.ru/tpost/p6d6ufj4c1-ierarhiya-metrik
    * [консалтинг про NSM и пирамиды метрик](https://datalatte.ru/#education)
 * [Mixpanel. Product benchmarks report 2017. 572 продукта, 1.3 млрд пользователей, 50 млрд событий. Usage, retention, engagement, conversion](https://discover.mixpanel.com/rs/461-OYV-624/images/2017-Mixpanel-Product-Benchmarks-Report.pdf)
 * [Data points: what should your startup measure? BOBBY PINERO. 2017]()
 * [The Foundation for Great Analytics is a Great Taxonomy. Amplitude. Ottavi-Brannon. 2017](https://amplitude.com/blog/event-taxonomy)
 * [Процесс построения новых метрик для a/b-тестирования. Роман Будылин. Матемаркетинг. 2018](https://www.youtube.com/watch?v=-e3PqKqG2CU)
 * [гайд по метрикам Артём чистяков](https://cloud.mail.ru/public/Q5RT/r26ZLRuJJ)
 * [Как просто отследить тренды поисковых запросов в Яндексе и Гугле - BRAIN-ON! Интернет маркетинг от А до Я ](https://seo-ru.com/blog/seo/kak-prosto-otsledit-trendyi-poiskovyih-zaprosov-v-yandekse-i-gugle#wordstat)

## Важность аналитики

 * Знаем, как сейчас дела
 * Определяем, куда развиваться проекту
 * Понимаем, как развиваться
 * Быстро понимаем, что идёт не так
 * Можем строить прогнозы
 * Изучаем свою аудиторию и клиентов
 * Делаем подходящий продукт


 * стадии зрелости аналитики в организациях
    * Решения принимают интуитивно. Аналитику не собирают
    * Собирают всё, до чего можно дотянуться, часто без понимания целей и причин
    * Встроены стандартные счётчики. Смотрят набор стандартных отчётов
    * Определяют цели, понимают связь метрик и целей. Проводят анализ влияния продуктовых изменений на цели
 * аналитический цикл
    * Строим модель работы продукта
    * предсказываем, что будет происходить
    * проверяем предсказание
    * формируем и проверяем гипотезы почему не сбылось
    * Уточняем модель работы продукта
 * понимаем для чего нужна конкретная метрика прежде чем тратить на неё время

## статья [Смотри, куда идешь, а то придешь, куда смотришь Елена Серёгина](https://medium.com/@elenest/смотри-куда-идешь-а-то-придешь-куда-смотришь-13886f756bf6)

 * Обычно бизнес-цели сводят к двум:
    * рост прибыли (рост Profit margin)
    * привлечение инвестиций (рост Retention)
    * Нередко у фирмы линейная комбинация из этих целей.
 * Растить прибыль надо осознанно. Поэтому нужны метрики. Аналитика становится основой стратегии.
 * чаще всего, когда CEO хотят одного, инвесторы другого, а менеджеры третье - получается крах.
 * стратегия для роста прибыли и возвращаемости:
    * Акцент на качестве продукта (счастье пользователей)
    * Акцент на ценообразовании
 * метрики
    * счастье пользователей. Мало кто понимает.
    * Качество продукта умеют мерить только для очень больших продуктов. 
    * Ценообразование штука не гибкая. Дороже продаем — выше маржа, дешевле продаем — больше продаж.
    * Скажи мне, какая у тебя любимая метрика, и я скажу, куда ты двигаешь свой продукт.
 * два типа руководителей:
    * LTV 
        * растят деньги от пользователя
        * retention проще не уронить, чем поднять
    * Качество 
        * растят качество для пользователя
        * Не ясно, как правильно мерить качество
        * Измерения качества продукта почти всегда требуют дополнительных инвестиций
        * Как качество связано с прибылью или хотя бы возвращаемостью обычно для всех большая загадка
 * мы идем обычно туда, куда смотрим
    * Метрики определяют направление развития продукта и бизнеса
    * цена — это часть продукта
    * пользователи реагируют на цену как на фичу
    * можно измерять качество в деньгах
    * можно измерять LTV в единицах качества (ну например, метрика качества поиска — число релевантных ответов на запросы)

## Метрики: типы и виды

 * метрика
    * Показатель эффективности продукта или отдельной фичи
    * Свершившееся и зафиксированное событие
 * виды
    * абсолютная/относительная
    * качественная/количественная
 * типы
    * Маркетинговые: Привлечение
    * Продуктовые: Взаимодействие
    * Бизнес
    * Технические: Работоспособность

## Бизнес-метрики

* Revenue
* Earnings/Profit
* EBITDA
* ARPU, ARPA, ARPC
* COGS

## Маркетинговые метрики

* Стоимость за действие
    * = стоимость / количество действий за период
    * Cost per click (CPC) — стоимость за клик
    * Cost per mille (CPM) — стоимость за 1 000 показов
    * Cost per action (CPA) — стоимость за действие
    * Cost per install (CPI) — стоимость за установку
    * Cost per lead (CPL) — стоимость за лид
* CTR 
    * Эффективность рекламы
    *  click-through rate — кликабельность
    * = (клики / показы)*100%
* CR
    * Conversion Rate
    * Конверсии в действия
    * = (уникальных регистраций / трафик ) * 100%
* CAC
    * Customer Acquisition Cost
    * см [Стоимость приобретения клиента](./юнит_экономика.md#Стоимость-приобретения-клиента)
    * = стоимость / количество привлечённых
* Окупаемость расходов
    * = ((выручка − затраты)/затраты)*100%
    * ROI — return on investments
    * ROMI — return on marketing investments
    * ROAS — return on ad spendings
    * ![](./аналитика_и_продуктовые_метрики/пример_ROI.jpg)

## Продуктовые метрики

 * Удержание и отток
    * customer retention rate (CRR) 
        * уровень удержания клиентов
        * Какой процент клиентов повторно взаимодействовал с проектом спустя время
        * = ((количество на конец периода − количество новых за период)/количество на начало периода)*100%
    * Churn rate
        * уровень оттока клиентов
        * = (количество потерянных клиентов/количество клиентов на начало периода)*100%
    * Retention rate
        * возвращаемость пользователей в проект или ключевые взаимодействия пользователей на проекте
        * Rolling Retention — процент пользователей, зашедших в период N+
        * N-day Retention — процент пользователей, зашедших в период N
            * ![](./аналитика_и_продуктовые_метрики/nday_retention.jpg)
        * Bracket Retention — процент пользователей, зашедших за период (неделя, месяц, свой период)
        * Оцениваем Product/Market fit
			* https://trends.google.ru/
            * ![](./аналитика_и_продуктовые_метрики/пример_retention.jpg)
        * формируется ли у пользователя привычка
        * Смотрим на эффективность привлечения
        * Изучаем дополнительные возможности коммуникации и монетизации
        * исключения: Продукты с длинным циклом потребления
 * NPS, CSI - Удовлетворённость
 * Lifetime, LTV, CLV - Срок жизни пользователя, выручка на одного пользователя за этот срок


## Метрика Retention

 * ценность: Какое действие пользователи делают каждый раз, возвращаясь в ваш продукт?
 * событие: Какую основную метрику вы отслеживаете и какое действие с ней связано?
 * Как определить интервал отслеживания
    * Выберите пользователей, которые совершали ключевое действие 2 раза за период
    * ключевое действие функциональное, а не просто авторизация
    * Измерьте, сколько времени пользователям требуется на совершение второго действия
    * Возьмите время, в течение которого 80% пользователей совершают действие повторно
    * Для упрощения анализа округлите до дней, недель, месяцев
    * ![](./аналитика_и_продуктовые_метрики/пример_интервал_retention.jpg)
 * N-day Retention
    * Только для новых пользователей или продуктов с ежедневным потреблением.
    * Понимание новых пользователей и их интерес к ценностному предложению
 * Rolling Retention
    * Хорошо подходит для продуктов с нерегулярным использованием. 
    * Вовлечённость пользователя в продукт
 * ![](./аналитика_и_продуктовые_метрики/сравнение_retention.jpg)


## Метрики LTV и CLV

 * много вариантов расчёта, нет стандарта
 * расстояние между первой и крайней покупкой или функциональным действием
 * частые ошибки
    * Сложно предсказать для нового продукта
    * Предсказания по прошлому не учитывают изменений продукта
    * Показатель разный для разных сегментов аудитории. 
    * Средние оценки могут вводить в заблуждение — необходим когортный анализ
    * не учитываются циклические интервалы отсутствия действий пользователя: отпуск, выходные, будни, болезнь, аврал
    * не отделять внезапные возвраты после большого перерыва
 * LTV 
    * lifetime value
    * совокупную выручку, от одного клиента за всё время взаимодействия с ним
    * LTV = Lifetime * ARPU
    * LTV = Средний чек * Число продаж в месяц * Lifetime
    * LTV = Revenue/Clients
 * CLV
    * Customer Lifetime Value
    * CLV = LTV * Маржинальность
    * CLV = Средний чек * Маржинальность * Число продаж в месяц * Lifetime
    * CLV = (Revenue - Advertising Costs)/Clients
 * график стоимость-время
    * красное - отрицательный доход, зелёный - положительный
    * ![](./аналитика_и_продуктовые_метрики/график_метрик.jpg)
    * две разные траектории для двух разных стартовых CAC
    * ![](./аналитика_и_продуктовые_метрики/график_метрик_1.jpg)
    * поменяли цену
    * ![](./аналитика_и_продуктовые_метрики/график_метрик_2.jpg)
    * уменьшаем отток, отдаляем дату крайней покупки
    * ![](./аналитика_и_продуктовые_метрики/график_метрик_3.jpg)
    * увеличили количество продаж
    * ![](./аналитика_и_продуктовые_метрики/график_метрик_4.jpg)

## Метрика NPS

 * NPS - Net Promoter Score
 * Индекс потребительской лояльности, процент фанатов
 * привлечение лидов, работа с негативными отзывами
 * Вопрос клиентам о готовности рекомендовать нашу компанию
 * Ответ по десятибалльной шкале
    * 1-6 критики detractor
    * 7-8 хатаскрайники neutral
    * 9-10 промоутеры promoter
 * = (Количество промоутеров — Количество критиков) / Общее количество опрошенных
    * 0-30% плохо
    * 30-50% средне
    * 50-100% хорошо
 * первые две категории должны перетекать со временем во третью
 * важно выбрать минимальный промежуток между опросами, чтобы не напрягать
 * проводить опрос после целевого действия а не "на дату"
 * проводить опрос после достаточно большого времени для ознакомления с продуктом

## CSI/CSAT

 * CSI/CSAT - Customer Satisfaction Index
 * Индекс удовлетворённости пользователей
 * улучшение качества продукта
 * оценка по 5 балльной шкале
    * удобство
    * ассортимент
    * цена
    * скорость доставки
 * оценка по семибалльной шкале
    * оцените важность этого параметра для вас
    * оцените удовлетворённость
    * важность умножаем на удовлетворённость, это весовые коэффициенты
    * ![](./аналитика_и_продуктовые_метрики/важность_удовлетворённость.jpg)
 * спрашивать про разные функции после совершения целевого действия, а не про все сразу

## метрики использования

 * Показывают, как пользователи взаимодействуют с продуктом
    * Количество просмотренных предложений
    * Кликабельность страниц
    * Количество использований фильтров


## Когорты

 * Они объединены по одному признаку во времени
 * необходимо учесть задержку от привлечения до покупки, инерцию привлечения
 * ![](./аналитика_и_продуктовые_метрики/когорты_пример_1.jpg)
 * ![](./аналитика_и_продуктовые_метрики/когорты_пример_2.jpg)
 * сезонность 5/2
 * ![](./аналитика_и_продуктовые_метрики/когорты_пример_3.jpg)
 * см [Когортный анализ](./pnl_экономика.md#когортный-анализ)
 * см [Считаем P&L](./pnl_экономика.md#считаем-pl)

## Основные принципы работы с метриками

 * необходимо договариваться об одинаковом понимании и способе расчёта метрик с коллегами
 * В марте конверсия в оплату упала на треть, мы сломали продукт
    * На самом деле: в марте маркетинг привлёк много нецелевого трафика
    * ![](./аналитика_и_продуктовые_метрики/интерпретация_метрик_1.jpg)
 * Минимально необходимые данные для принятия решений
    * Задача — проанализировать потенциальную эффективность фичи
    * Найти исторические данные, которые помогут сделать прогноз — 5 часов
    * Привлечь разработку для сбора дополнительных данных — 8 часов
    * Подготовить скрипт для обработки и всё просчитать — 8 часов
    * итого 21 час
    * можно посчитать не полностью, этого будет достаточно: 2 дня теста, 2 часа на проработку и 1 час на анализ

## иерархия/пирамида метрик

 * Определиться с целью, чтобы что
 * строить итеративно, под текущую задачу
 * Определить набор ключевых показателей в достижении цели:
    * метрики этапов взаимодействия с продуктом
    * метрики процессов в продукте
    * метрики самого продукта
    * использование фреймворков
 * Приоритизировать
    * иерархия
    * понимание зависимостей метрик
 * пирамида метрик
    * наверху результат, внизу рычаги влияния - нельзя наоборот!
    * умножаем или складываем соседние
    * ![](./аналитика_и_продуктовые_метрики/пирамида_метрик.jpg)

## NSM

 * синтетическая метрика, увеличение которой дает наибольшее влияние на рост всего продукта. Помогает сфокусировать команду на самом главном
 * примеры
	* Facebook Доля пользователей, добавивших 7 друзей в первые 10 дней.
	* Zoom Количество еженедельных встреч на аккаунт.
	* Headspace Доля новичков, включивших ежедневные напоминания о медитации.
 * метрика с самым большим коэффициентом влияния на прибыль, в выбранном промежутке времени
 * Наличие NSM помогает развивать продукт во многих направлениях, обеспечивая ему устойчивый рост, 
 * позволяет лучше понимать своих пользователей, увеличивать вероятность acquisition, конверсии в клиента, например через яндекс.вебвизор
 * одна метрика, которая указывает, что бизнес движется в правильном направлении
 * Метрика должна показывать, что пользователь оценил основную ценность продукта — попробовал и провзаимодействовал
 * Метрика должна отражать вовлечённость пользователя и уровень активности
 * Метрика должна быть лёгкой для понимания и использования внутри компании
 * Идеальная метрика с первого раза — самообман. Может потребоваться несколько итераций, пока вы отыщете «ту самую» метрику
 * ![](./аналитика_и_продуктовые_метрики/nsm_пример.jpg)

## KPI

 * основные KPI
	* конверсия в платящего
	* чистая прибыль
	* процент оттока
	* количество лидов
 * формат KPI
	* название
	* значение(не менее, не более, точно)
	* период достижения
	* вспомогательные метрики
 * ![](./аналитика_и_продуктовые_метрики/kpi.jpg)


## фреймворк TEMA

 * target it 
    * определите цель
    * для чего она нужна, чтобы что сделать?
    * формат [user story](./backlog.md#user-story-пользовательские-истории)
 * explore it 
    * узнайте о цели больше
    * возможные показатели, маркеры - нельзя пропускать этап!
    * критерии выбора показателей
 * metric it 
    * подберите метрики под маркеры
    * конкретное значение
    * можно сравнивать за разные периоды
    * можно посчитать, есть система сбора
 * analyse it 
    * проанализируетй метрики
 * ![](./аналитика_и_продуктовые_метрики/пример_TEMA.jpg)
 * ![](./аналитика_и_продуктовые_метрики/пример_TEMA_2.jpg)

