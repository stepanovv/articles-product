# Справочная


 * earning - доход
 * revenue - прибыль
 * омниканальность 
    * взаимодействие с пользователем по разным каналам через один сервис
    * чат, сайт, портал, оффлайн, телефон
 * PnL это управленческая отчетность, которая нужна для упраления продуктом
    * profit and loss statement
    * выручка
	* себестоимость продаж
	* валовая прибыль
	* расходы/ФОТ
	* прибыль до налогов
 * DAU, WAU, MAU, YAU
    * Daily active users
 * LTV
    * см [Метрики LTV и CLV](./аналитика_и_продуктовые_метрики.md#Метрики-LTV-и-CLV)
    * lifetime value
    * деньги с клиента за длительный период
    * LTV=AMPPU
    * CLV - customer LTV
    * CLV = CLTV = LTV * Маржинальность
 * см ## воронка пользователя
 * margin profit
    * Margin profit 
    * прибыльность
    * доля прибыли, после вычета всех затрат (включая переменные и постоянные издержки) от общего дохода. 
    * = ((общий доход + общие издержки) / деленная на общий доход) * 100
 * revenue 
    * выручка
    * = average price * payments
 * gross profit
    * валовая прибыль
    * ?? = AMPPU * buyers
    * = revenue - COGS
 * COGS
    * cost of goods sold
    * себестоимость товаров и услуг
    * = запасы (начало периода) + расходы на производство − запасы (конец периода)
    * revenue - cogs = gross profit
    * динамические расходы на всё кроме продажи/поставки
    * не входят затраты на основные фонды, аренду и ФОТ
    * примеры:
        * скидки
        * демо сессии
        * интеграция
        * закупка товаров на склад
        * бонусы, привязанные к продажам
        * эквайринг, комиссия за продажу
        * доставка, упаковка
 * C - conversion, %
 * C1
    * конверсия нового/привлечённого пользователя, с первой покупки
    * = (Количество покупателей / количество посетителей (пользователей)) * 100
 * UA - User Acquisition
 * CPLead 
 * CPUser 
    * Cost per User
    * стоимость привлечения посетителя/пользователя
    * = бюджет / количество пользователей
    * включая не платящих
 * CAC 
    * см [Стоимость приобретения клиента](./юнит_экономика.md#Стоимость-приобретения-клиента)
    * Customer Acquisition Cost
    * стоимость привлечения платящего пользователя
 * CPA
    * Cost Per Acquisition
    * частная стоимость привлечения
    * = AMPU - CPA
    * = revenue/() - CPU
 * AP
    * average price
    * средний чек
 * APC 
    * average payment count
    * среднее количество покупок за период времени
    * = 1.2
 * AMPU
    * average margin per user
    * ? устаревшая метрика, без COGS
    * маржа с нового/привлечённого пользователя за период
    * = C1 * AMPPU
    * %
 * AMPPU 
    * average margin per paying user
    * маржа с платящего пользователя
    * = average price * margin * average payment count
 * ARPPU
    * устарело, не учитывает cogs, менее точная
    * average revenue per paying user
    * средняя выручка с платящего пользователя
    * = revenue / paying users
 * ARPU
    * устарело, не учитывает cogs, менее точная
    * average revenue per user
    * средняя выручка с привлечённого пользователя
    * = revenue / users
    * = RPV - revenue per visitor
 * ARPA - Average revenue per account (ARPA) — средняя выручка на аккаунт
 * ARPU 
    * Average revenue per user (ARPU) — средняя выручка на пользователя
    * = revenue / active users
 * ARPC - Average revenue per client (ARPC) — средняя выручка на клиента
 * CPC 
    * Cost Per Click
    * стоимость за клик на рекламную ссылку
    * = стоимость рекламной кампании / количество кликов
 * CPM 
    * Cost Per Mille
    * стоимость за 1000 показов
    * CPM - это метрика, указывающая на стоимость тысячи показов рекламы
    * = стоимость рекламной кампании / 1000
 * [ROI](https://practicum.yandex.ru/blog/chto-takoe-roi-i-kak-ego-schitat/)
	* ROI = ((Доход - Затраты) / Затраты) * 100% 
		* эффективность все инвестиций, включая затраты на производство
		* Return on investment
		* возврат инвестиций
	* ROMI = (Доход - расход на маркетинг) / Расход на маркетинг * 100%
		* эффективность продвижения/маркетинга
		* Return on marketing investment
	* ROAS = (Доход от рекламы / Расход на рекламу) * 100%
		* рентабельность, заработал ли бизнес больше, чем потратил на рекламу/маркетинг
		* для сравнения продуктов
		* Return on Advertising Spend
		* окупаемость расходов на рекламу


